import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import { useEffect } from 'react'

export default function (props) {
  useEffect(() => {
    props.updateTitle('Home')
  }, [])

  return (
    <Container>
      <Typography variant='h5' component='h1' gutterBottom>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla faucibus lorem a est viverra, quis mattis dolor commodo. Aliquam eros lacus, lacinia id efficitur id, eleifend sit amet lectus. Mauris ut vehicula nisi. In in est efficitur, tempor turpis quis, eleifend metus. Nam congue diam eu erat finibus laoreet. Nunc fermentum hendrerit felis sagittis finibus. Curabitur varius dui quis dictum consequat. Proin imperdiet nisl nec nunc facilisis, vel tristique tortor blandit.
        </p>
        <p>
        Cras ac massa at eros semper maximus. Maecenas vel sagittis mauris. Mauris a risus et tellus tempor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu nunc ac est pulvinar mattis nec sed orci. Aliquam erat volutpat. Proin a sapien consectetur, bibendum quam feugiat, ultricies mi. Pellentesque tincidunt, elit quis feugiat tempus, nisi dolor cursus urna, ac lobortis sapien massa vitae dui. Vivamus orci purus, condimentum vitae viverra sed, mattis sed ipsum. Sed lacus felis, iaculis nec sapien eu, congue laoreet nunc.
        </p>
        <p>
        Vivamus quis lacinia urna. Nulla feugiat rutrum semper. Cras consequat volutpat enim at bibendum. Ut viverra ante vel nulla finibus ullamcorper. Donec interdum laoreet nisi in viverra. Ut egestas consectetur euismod. Pellentesque pulvinar felis sit amet ex ullamcorper, vitae pharetra urna dignissim.
        </p>
        <p>
        Vestibulum sodales porttitor lectus, at elementum lacus interdum in. Integer id lectus posuere, varius augue sit amet, rutrum enim. Vivamus ac nulla mauris. Donec nisi purus, mattis non sem posuere, tempor dictum est. Aliquam ultrices tincidunt est, sed eleifend augue ullamcorper eget. Aenean ut cursus orci. Suspendisse sodales vel risus sit amet consectetur. Pellentesque sem orci, porttitor vitae venenatis sit amet, scelerisque id nunc. Vestibulum ullamcorper rhoncus lectus, a ultrices diam convallis quis.
        </p>
        <p>
        Donec turpis ante, convallis non interdum a, ultricies auctor urna. Sed eget felis lacus. Nullam nec molestie dolor. Aliquam id massa et turpis auctor aliquam. Pellentesque congue sollicitudin eros, non iaculis enim posuere ac. Pellentesque metus nulla, dignissim eget convallis nec, convallis eu orci. Phasellus interdum vitae urna ut blandit. Ut mollis, lacus a congue pretium, velit quam semper eros, et congue sapien diam quis ante. Cras nulla ante, hendrerit sed ipsum sed, consequat dapibus lorem. Praesent ante erat, interdum id varius a, luctus at augue. Etiam ullamcorper mollis efficitur. Morbi suscipit felis eget rutrum placerat. Sed ac accumsan risus. Vivamus in turpis eget elit convallis convallis id vel nulla.
        </p>
      </Typography>
    </Container>
  )
}
