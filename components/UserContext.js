import { createContext } from 'react'

export default createContext({
  user: null,
  signIn: null,
  signOut: null
})
